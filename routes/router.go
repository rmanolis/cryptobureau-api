package routes

import (
	"bitbucket.org/rmanolis/cryptobureau-cli/cbdsa"
	"crypto/rand"
	"github.com/gin-gonic/gin"
	"net/http"
)

type Key struct {
	PrivateKey string "json:'private_key'"
	PublicKey  string "json:'public_key'"
}

func Generate(c *gin.Context) {
	curve := c.Param("curve")

	keys, err := cbdsa.GenerateKeys(curve, rand.Reader)
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	key := Key{
		PrivateKey: string(keys.PrivateKey),
		PublicKey:  string(keys.PublicKey),
	}
	c.JSON(http.StatusOK, key)
}

type Verification struct {
	Text      string "json:'text'"
	PublicKey string "json:'public_key'"
	Signature string "json:'signature'"
}

type IsVerified struct {
	IsVerified bool "json:'is_verified'"
}

func Verify(c *gin.Context) {
	var verif Verification
	err := c.BindJSON(&verif)
	if err != nil {
		c.AbortWithError(http.StatusNotAcceptable, err)
		return
	}
	is_verified, err := cbdsa.Verify([]byte(verif.PublicKey), verif.Text, []byte(verif.Signature))
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	c.JSON(http.StatusOK, IsVerified{IsVerified: is_verified})
}

type SignThis struct {
	Text       string "json:'text'"
	PrivateKey string "json:'private_key'"
}

type Signature struct {
	Signature string "json:'signature'"
}

func Sign(c *gin.Context) {
	var sig SignThis
	err := c.BindJSON(&sig)
	if err != nil {
		c.AbortWithError(http.StatusNotAcceptable, err)
		return
	}

	sign, err := cbdsa.Sign(sig.Text, []byte(sig.PrivateKey), rand.Reader)
	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	c.JSON(http.StatusOK, Signature{Signature: string(sign)})

}

func LoadRoute(router *gin.Engine) {
	router.POST("/sign", Sign)
	router.GET("/generate/:curve", Generate)
	router.POST("/verify", Verify)
}
