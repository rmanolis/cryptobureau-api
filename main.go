package main

import (
	"bitbucket.org/rmanolis/cryptobureau-api/routes"
	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()
	routes.LoadRoute(router)
	router.Run(":8080") // listen and server on 0.0.0.0:8080
}
